# Render#
Render `html` using PYMES API
* Twig-1.35.2
* Config.php

# DEPLOY DEBIAN 9
Process to deploy this app in DEBIAN 9
## NEW DOMAIN
- Add to cloud.pymes.com.pe
- DNS CONFIGURE
- Generate SSL
- ENABLE IN MARCRO vhost-domains.conf
- Reset apache2

## USER
Create user `pymes`, with access to shell
```sh
useradd pymes -m
usermod -s /bin/bash pymes
usermod -aG sudo pymes
passwd -d pymes
export VISUAL=vim;
visudo
pymes ALL=(ALL) PASSWD:ALL #after %sudo
```
## Securing
```sh
vim /etc/ssh/sshd_config
PermitRootLogin no
PasswordAuthentication no
PermitEmptyPasswords no

mkdir /home/viajro/.ssh
cp .ssh/authorized_keys /home/viajro/.ssh/authorized_keys
chown viajro:viajro /home/viajro/.ssh/ -R
chmod 600 /home/viajro/.ssh/authorized_keys
service ssh restart
```
## CLONE PROYECT
```sh
su - pymes
git clone git@gitlab.com:pymes.com.pe/render.git
git clone git@gitlab.com:pymes.com.pe/templates.git
```
## INSTALL
```sh
apt-get install vim
apt-get install apache2
apt-get install libapache2-mod-php7.0

apt-get install git
apt-get -y install php7.0
apt-get install curl
apt-get install php7.0-curl
```

### APACHE 2 MODULE:
Active apache2 modules
```sh
a2enmod rewrite
a2enmod ssl
a2enmod headers
a2enmod macro
a2enmod expires
scp vhost-domains.conf /etc/apache2/sites-available
curl -I url # test headers
```

### Virtualhost
- /var/www/html
- Clear data from index.html

### SECURING
/etc/ssh/sshd_config
```sh
PasswordAuthentication no
PermitEmptyPasswords no

service ssh restart
```

### Firewall (UFW)
```sh
apt install -y ufw
systemctl restart ufw
ufw default deny incoming
ufw default allow outgoing
```

Add roles
```sh
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow 22/tcp
ufw enable
ufw status
```
Deny port
```sh
ufw deny IP
```
### Virtualhost configuration
Copy
- vhost-000-default.conf
- vhost-pymes-com-pe.conf
to /etc/apahce2/sites-availables
```sh
a2ensite 000-default.conf
a2ensite pymes-com-pe.conf
a2ensite domain.conf
```

### DIR for cache ###
create a directory for save twig cache and app cache
mkdir /home/pymes/tmp
chown www-data:www-data /home/pymes/tmp -R
chmod 744 /ome/pymes/tmp -R

## Install SSL Lets Encription
```sh
su - pymes
wget -O -  https://get.acme.sh | sh
# Close and re-open (ctrl + d)
58 0 * * * "/home/pymes/.acme.sh"/acme.sh --cron --home "/home/pymes/.acme.sh" > /dev/null
# new
58 2 5 * * "/home/pymes/.acme.sh"/acme.sh --cron --home "/home/pymes/.acme.sh" > /dev/null
58 3 15 * * "/home/pymes/.acme.sh"/acme.sh --cron --home "/home/pymes/.acme.sh" > /dev/null
58 4 25 * * "/home/pymes/.acme.sh"/acme.sh --cron --home "/home/pymes/.acme.sh" > /dev/null

mkdir -p /etc/apache2/2.4/ssl
chown pymes:pymes /etc/apache2/2.4
# DH
openssl dhparam -out /etc/apache2/2.4/ssl/dhparam.pem 2048
```
### Create Certificate
- Add domain to vhost-domains.conf
- Generate the certificate
- Add domain to vhost-domains-ssl.conf
```sh
acme.sh --issue -d pymes.com.pe -d www.pymes.com.pe -w /home/pymes/render/public

acme.sh --install-cert -d pymes.com.pe \
--cert-file /etc/apache2/2.4/ssl/pymes.com.pe-cert.pem \
--key-file /etc/apache2/2.4/ssl/pymes.com.pe-key.pem \
--fullchain-file /etc/apache2/2.4/ssl/pymes.com.pe.pem \
--reloadcmd "sudo systemctl reload apache2"
```

### Test SSL
https://www.ssllabs.com/ssltest/

### X-FRAME
https://tools.geekflare.com/report/x-frame-options-test/
