<?php
/**
* author:@erajuan
* company:pymes.com.pe
*/
require_once "../core/web.php";

$server_name = $_SERVER["SERVER_NAME"];

$web = new Web($server_name);
$web->info();
/**
* Required
* - Domain ->domain()
* - Protocol ->protocol()
* - Languages -> languages()
*/
header("Content-Type: text/plain");
?>
#
# robots.txt
#
User-agent: *
Disallow: contact_us.php
Disallow: robots.php
Disallow: editar.php
Disallow: editar
<?php foreach ($web->languages() as $lang): ?>
Sitemap: <?php print $web->protocol() . $web->domain() . $lang['url'] ?>sitemap.xml
<?php endforeach ?>