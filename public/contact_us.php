<?php
require "../core/gump.class.php";
require "../core/forms.send.php";

if (isset($_POST["form_url"])) {
    $gump = new GUMP();
    $_POST = $gump->sanitize($_POST);

    $gump->validation_rules(array(
        'full_name'    => 'required|max_len,96|min_len,4',
        'email'       => 'required|max_len,96|valid_email',
        'phone'      => 'max_len,32',
        'subject'      => 'required|max_len,64|min_len,4',
        'message'      => 'required|max_len,480|min_len,16',
        'pyme_s'      => 'required|numeric|min_numeric,11|max_numeric,19',
        'pym_es'      => 'required|exact_len,1',
        'py_mes'      => 'exact_len,0'
    ));

    $gump->filter_rules(array(
        'full_name' => 'trim|sanitize_string',
        'email'    => 'trim|sanitize_email',
        'phone' => 'trim|sanitize_string',
        'subject' => 'trim|sanitize_string',
        'message' => 'trim|sanitize_string',
        'pyme_s' => 'trim|sanitize_string',
        'pym_es' => 'trim|sanitize_string',
        'py_mes' => 'trim|sanitize_string'
    ));

    $validated_data = $gump->run($_POST);
    if($validated_data === false) {
        print $gump->get_readable_errors(true);
        exit;
    } else {
        $send = new SendForm($_SERVER["SERVER_NAME"]);
        $res = $send->contactForm($validated_data);
        header("HTTP/1.1 204 No content");
        header("Location: " . $validated_data["form_answer"]);
        exit;
    }

} else {
    header("HTTP/1.1 204 No content");
    header("Location: /");
    exit;
}