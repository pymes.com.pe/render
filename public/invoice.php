<?php
/**
* Invoice
* autor: @erajuan
*/
require_once "../core/invoice.php";

$server_name = $_SERVER["SERVER_NAME"];
$request_uri = $_SERVER["REQUEST_URI"];
$code = '0';
$token = "";
if (isset($_GET['i']))
{
    $code = $_GET['i'];
}
if (isset($_GET['token']))
{
    $token = $_GET['token'];
}
// Render web site
$invoice = new Invoice($server_name,$request_uri);
if ($token)
{
    print_r($invoice->charge($token));
} else {
    print $invoice->start($code);
}