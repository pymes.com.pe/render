<?php
require "../core/gump.class.php";
require "../core/forms.send.php";

if (isset($_POST["form_url"]))
{
    $gump = new GUMP();

    $_POST = $gump->sanitize($_POST);

    $gump->validation_rules(array(
        'full_name'     => 'required|max_len,96|min_len,4',
        'email'         => 'required|max_len,96|valid_email',
        'phone'         => 'alpha_space|max_len,32',
        'country'         => 'alpha_space|max_len,32',
        'tour'          => 'required|max_len,64|min_len,6',
        'journey_date'  => 'required',
        'adults'        => 'required|integer|min_numeric,0|max_numeric,50',
        'node_id'        => 'integer|min_numeric,0',
        'children'         => 'required|integer|min_numeric,0|max_numeric,50',
        'message'       => 'required|max_len,480|min_len,16'
    ));

    $gump->filter_rules(array(
        'full_name' => 'trim|sanitize_string',
        'email'    => 'trim|sanitize_email',
        'phone' => 'trim|sanitize_string',
        'country' => 'trim|sanitize_string',
        'tour' => 'trim|sanitize_string',
        'node_id' => 'trim|sanitize_numbers',
        'adults' => 'trim|sanitize_numbers',
        'children' => 'trim|sanitize_numbers',
        'message' => 'trim|sanitize_string'
    ));

    $validated_data = $gump->run($_POST);

    if($validated_data === false)
    {
        echo $gump->get_readable_errors(true);
        exit;
    } else {
        $send = new SendForm($_SERVER["SERVER_NAME"]);
        $res = $send->bookTour($validated_data);
        header("HTTP/1.1 204 No content");
        header("Location: " . $validated_data["form_answer"]);
        exit;
    }

} else {
    header("HTTP/1.1 204 No content");
    header("Location: /");
    exit;
}