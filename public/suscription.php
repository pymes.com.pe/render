<?php
require "../core/gump.class.php";
require "../core/forms.send.php";

if (isset($_POST["form_url"]))
{
    $gump = new GUMP();

    $_POST = $gump->sanitize($_POST);

    $gump->validation_rules(array(
        'email'     => 'required|max_len,96|valid_email'
    ));

    $gump->filter_rules(array(
        'email'    => 'trim|sanitize_email'
    ));

    $validated_data = $gump->run($_POST);

    if($validated_data === false)
    {
        echo $gump->get_readable_errors(true);
        exit;
    } else {
        $send = new SendForm($_SERVER["SERVER_NAME"]);
        $res = $send->suscription($validated_data);
        header("HTTP/1.1 204 No content");
        header("Location: " . $validated_data["form_answer"]);
        exit;
    }

} else {
    header("HTTP/1.1 204 No content");
    header("Location: /");
    exit;
}