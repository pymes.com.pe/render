<?php
/**
* author:@erajuan
* company:pymes.com.pe
*/
require_once "../core/web.php";

$request_uri = $_SERVER["REQUEST_URI"];
$server_name = $_SERVER["SERVER_NAME"];

$web = new Web($server_name);
$web->info();

/**
* Required
* - Domain ->domain()
* - Protocol ->protocol()
* - Languages -> languages()
*/
header("Content-type: application/xml");
print '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<?php foreach ($web->sitemap($request_uri) as $link): ?>
<url>
    <loc><?php print $link->url; ?></loc>
    <changefreq>weekly</changefreq>
    <priority><?php print $link->priority; ?></priority>
</url>
<?php endforeach ?>
</urlset>