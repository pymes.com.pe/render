<?php
/* *
* by: @erajuan
*/
require "../core/gump.class.php";
require "../core/web.php";

$gump = new GUMP();

$_GET = $gump->sanitize($_GET);

$gump->validation_rules(array(
    'a' => 'required|numeric',
    'c' => 'required|exact_len,32'
));

$gump->filter_rules(array(
    'a' => 'trim|sanitize_numbers',
    'c' => 'trim'
));

$validated_data = $gump->run($_GET);

if($validated_data === false)
{
    header("HTTP/1.1 204 No Content");
} else {
    $web = new Web($_SERVER["SERVER_NAME"]);
    $web->downloadInfo($validated_data["a"], $validated_data["c"]);
}
header("Location: /?level=6");
exit;

?>