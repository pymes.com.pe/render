<?php
// Notificar todos los errores excepto E_NOTICE
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(NULL);
/**
* Render Web Sites
* autor: @erajuan
*/
require_once "../core/render.php";

$level = 0;
$server_name = $_SERVER["SERVER_NAME"];
$request_uri = $_SERVER["REQUEST_URI"];
$getVars = array("item"=>"");

if ( isset($_GET["item"]) )
{
    $getVars["item"] = filter_input( INPUT_GET, 'item', FILTER_SANITIZE_SPECIAL_CHARS );
}
$getVars["node_id"] = 1;
if ( isset($_GET["node_id"]) )
{
    $getVars["node_id"] = filter_input( INPUT_GET, 'node_id', FILTER_SANITIZE_NUMBER_INT );
}
if ( isset($_GET["level"]) )
{
    if(is_numeric($_GET["level"]))
    {
        $level = $_GET["level"];
    }
}

// Render web site
$web = new Render($server_name,$request_uri);
print $web->start($level, $getVars);