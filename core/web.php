<?php
/**
* Web
* autor: @erajuan
* company: pymes.com.pe
*/

require_once "config.php";

class Web
{
    private $_pk;
    private $_code;
    private $_domain;
    private $_ssl;
    var $status;
    var $robot_index;
    var $company_type;
    var $name;
    var $short_name;
    var $phone;
    var $phones;
    var $mobile;
    var $mobiles;
    var $email;
    var $email_alt;
    var $multi_language;
    var $with_www;
    var $language;
    var $languages;
    var $visa_link;
    var $visa_commission;
    var $template;
    var $color;
    var $hex;
    var $file_css;
    var $file_js;
    var $environment;
    var $logo;
    var $favicon;

    function __construct($domain)
    {
        $this->_domain = str_replace("www.","",$domain);
    }
    /**
    * Build API URL
    */
    public function api($url="")
    {
        if ($url)
        {
            $api = API_SERVER . "api/" . $this->code() . "/" . $this->pk() . $url;
            return $api;
        } else {
            $api = API_SERVER . "api/" . $this->code() . "/" . $this->pk();
            return $api;
        }
    }
    /**
    *
    */
    public function cacheDir()
    {
        $path = WEB_CACHE . $this->domain() . "/" ;
        return $path;
    }
    public function path($file_name = null)
    {
        if ($file_name)
        {
            return $this->cacheDir() . $file_name . ".json";
        } else {
            return $this->cacheDir();
        }
    }
    /**
    *
    */
    private function infoPath()
    {
        $path = $this->path("info");
        return $path;
    }
    /**
    * Get web basic information
    */
    public function info()
    {
        $url = $this->infoPath();
        if(file_exists($url))
        {
            $response = file_get_contents($this->infoPath());
            $web = json_decode($response);
            $this->_pk = $web->pk;
            $this->_domain = $web->domain;
            $this->_code = $web->code;
            $this->_ssl = $web->ssl;
            $this->status = $web->status;
            $this->robot_index = $web->robot_index;
            $this->name = $web->name;
            $this->short_name = $web->short_name;
            $this->phone = $web->phone;
            $this->phones = $web->phones;
            $this->mobile = $web->mobile;
            $this->mobiles = $web->mobiles;
            $this->email = $web->email;
            $this->email_alt = $web->email_alt;
            $this->multi_language = $web->multi_language;
            $this->with_www = $web->with_www;
            $this->language = $web->language;
            $this->languages = $web->languages;
            $this->visa_link = $web->visa_link;
            $this->visa_commission = $web->visa_commission;
            $this->template = $web->template;
            $this->color = $web->color;
            $this->hex = $web->hex;
            $this->file_css = $web->file_css;
            $this->file_js = $web->file_js;
            $this->environment = $web->environment;
            $this->logo = $web->logo;
            $this->favicon = $web->favicon;
        } else {
            $this->_status = 404;
        }
    }
    /**
    * Download  basic web information: WEB_PK, WEB_DOMAIN, WEB_CODE
    */
    public function downloadInfo($pk=null, $code=null)
    {
        if (! is_null($pk) and  ! is_null($code)){
            $this->_pk = $pk;
            $this->_code = $code;            # code...
        }
        $url = $this->api("/info");
        // Download
        $response = file_get_contents($url);

        $web = json_decode($response);
        if ($this->domain() === $web->domain) // Domain is iqual
        {
            $dir = WEB_CACHE . $this->domain();
            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }
            file_put_contents($this->infoPath(), $response);
        }
        return true;
    }
    /**
    * author: @erajuan
    * date: 12-MAY-2017
    */
    public function sitemap($request_uri)
    {
        if ($request_uri === "/sitemap.xml")
        {
            $url = $this->api("/sitemap");
        } else {
            $temp = explode("/", $request_uri);
            $lang_code = $temp[1];
            $url = $this->api("/sitemap/" . $lang_code . "/");
        }
        $response = file_get_contents($url);
        $content = json_decode($response);
        return $content->objs;
    }
    public function domain()
    {
        return $this->_domain;
    }
    public function pk()
    {
        return $this->_pk;
    }
    public function code()
    {
        return $this->_code;
    }
    public function protocol()
    {
        if ( $this->_ssl )
        {
            return "https://";
        }
        return "http://";
    }
    public function ssl()
    {
        return $this->_ssl;
    }
    public function uri()
    {
        return $this->_uri;
    }
    public function languages()
    {
        $languages = Array();
        foreach ($this->languages as $lang_code)
        {
            $e = array(
                "title" => strtoupper($lang_code),
                "lang_code" => $lang_code,
                "url" => "/" . $lang_code . "/"
                );
            $languages[] = $e;
        }
        return $languages;
    }
    public function clearAllCache()
    {
        $files = glob($this->cacheDir() . "nodes_*.json");
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }
    }
    public function www()
    {
        return $this->with_www;
    }
    public function getVisa()
    {
        return $this->visa_link;
    }
    public function getVisaCommission()
    {
        return $this->visa_commission;
    }
    public function acceptVisa()
    {
        if ( $this->visa_link )
            return true;
        return false;
    }
}