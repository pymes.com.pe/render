<?php
/**
* Render: Render templates
* autor: @erajuan
* company: pymes.com.pe
*/
require_once '../Twig/lib/Twig/Autoloader.php';
require_once "config.php";
require_once "web.php";
require_once "page.php";

function renderSafe($field) {
    return $field;
}

function renderEmail($field) {
    return str_replace("@", "<i class='fa fa-at'></i>", $field);
}

function renderFA($field) {
    $t = explode('|', $field);
    if (count($t)==2) {
        return '<span class="icon"><i class="fa fa-' . $t[1] . '"></i></span> <span>' . $t[0] . '</span>';
    } else {
        return $t[0];
    }
}

class Render {
    private $robots_index; # Robot index
    private $web; # OBJECT: Domain, PK, Code, status
    # Domains status: 200-Exists, 404-Not register, 403-Suspende
    private $uri; # current url
    private $template; #
    private $language; # current language
    private $auto_reload = false;

    function __construct($server_name,$request_uri)
    {
        $this->uri = $request_uri;

        $web = new Web($server_name);
        $web->info();
        $this->web = $web;
    }
    /**
    * Directory for save template cache
    */
    private function cache()
    {
        if (CACHE) {
            $path = WEB_CACHE . "template/" .  $this->web->domain();
            return $path;
        }
        return CACHE;
    }
    function twig($ctx)
    {
        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem(TEMPLATES_DIR);
        $twig = new Twig_Environment($loader, array(
            'cache' => $this->cache(),
            'auto_reload' => $this->auto_reload
            ));
        $twig->disableDebug();
        $filterSafe = new Twig_SimpleFilter(
            'safe',
            'renderSafe',
            array('is_safe' => array('html')));
        $twig->addFilter($filterSafe);
        $filterFA = new Twig_SimpleFilter(
            'fa',
            'renderFA',
            array('is_safe' => array('html')));
        $twig->addFilter($filterFA);
        $filterEmail = new Twig_SimpleFilter(
            'email',
            'renderEmail',
            array('is_safe' => array('html')));
        $twig->addFilter($filterEmail);
        // Add context variables
        $ctx["static_url"] = STATIC_URL;
        // Verificar que existe node
        if ( $ctx["node"]->status == 200 )
        {
            if ( $ctx["amp"] )
            {
                $theme = $twig->render('amp/page-'.$ctx["node"]->node_type.'.twig.html', $ctx);
            }  else {
                try {
                    $theme = $twig->render($this->web->template.'/page-'.$ctx["node"]->node_type.'.twig.html', $ctx);
                } catch (Exception $e) {
                    $theme = $twig->render($this->web->template.'/page-p.twig.html', $ctx);
                }
            }
            return $theme;
        } else {
            $ctx["web"]->robot_index = false;
            return $twig->render($this->web->template.'/error.twig.html', $ctx);
        }
    }
    public function error()
    {
        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem(TEMPLATES_DIR);
        $twig = new Twig_Environment($loader, array(
            'cache' => false,
            'auto_reload' => true
            ));
        $filterFA = new Twig_SimpleFilter(
            'fa',
            'renderFA',
            array('is_safe' => array('html')));
        $twig->addFilter($filterFA);
        $filterSafe = new Twig_SimpleFilter(
            'safe',
            'renderSafe',
            array('is_safe' => array('html')));
        $twig->addFilter($filterSafe);
        $filterEmail = new Twig_SimpleFilter(
            'email',
            'renderEmail',
            array('is_safe' => array('html')));
        $twig->addFilter($filterEmail);
        // Add context variables
        $ctx["static_url"] = STATIC_URL;
        // Verificar que existe sitio web
        return $twig->render('errors/not_registered.twig.html', $ctx);

    }
    public function start($cacheLevel, $getVars)
    {
        // 1. Verify if domain exist
        if ($this->web->status == 200)
        {
            $page = new Page(
                $this->web,
                $this->uri
                );

            if ($cacheLevel == "3")
            {
                $this->auto_reload = true;
                $this->web->clearAllCache();
                $this->web->downloadInfo();
                $page->structureDownload();
                $page->nodeDownload();
            }
            if ($cacheLevel == "6")
            {
                $this->auto_reload = true;
                $page->structureDownload();
                $page->nodeDownload();
            }
            // 2. Get node object
            if ($cacheLevel == "2")
            {
                $this->auto_reload = true;
                $page->nodeDownload();
            }
            $node = $page->node();
            // 3. Get structure object
            $structure = $page->structure();
            // render template
            $ctx = array(
                'web' => $this->web,
                'node' => $node,
                'page' => $structure,
                'amp' => $page->isAmp(),
                'get' => $getVars,
                );
            return $this->twig($ctx);
        }
        return $this->error();
    }
}