<?php
/**
* Page
* autor: @erajuan
* company: pymes.com.pe
*/

class Page
{
    private $_uri;
    private $_language;
    private $_amp;
    private $web;
    private $_status;

    function __construct($web, $uri)
    {
        $this->web = $web;
        $this->_language = $web->language;
        $this->_amp = false;
        $this->_status = 404;
        $url = explode("?", $uri);
        $this->_uri = $url["0"];
        $this->setLanguage();
        $this->sanitizeUrl();
    }
    /**
    * Set Language
    */
    private function setLanguage()
    {
        $languages = array(
            "es"=>true,
            "en"=>true,
            "de"=>true,
            "pt-br"=>true
        );
        if (stripos($this->_uri, "wp-") === false)
        {
            if (stripos($this->_uri, "/admin") === false)
            {
        # GET language code
        if ($this->_uri != "/")
        {
            $temp = explode("/", $this->_uri);
            $lang = $temp[1];
            # 1509: asegurarse que este sea un idioma existente
            foreach ($this->web->languages as $language)
            {
                if ( $language == $lang )
                {
                    $this->_status = 200;
                    $this->_language = $lang;
                    break;
                }
            }
        }
            } else {
                $this->_status = 403;
            }
        } else {
            $this->_status = 403;
        }
    }
    /**
    * Build path to page json, structure data
    */
    private function structurePath($language = null)
    {
        if (is_null($language))
            $path = $this->web->path("nodes_" . $this->_language);
        else
            $path = $this->web->path("nodes_" . $language);
        return $path;
    }
    /**
    * Download web structure
    */
    public function structureDownload()
    {
        $current_language = "";
        foreach ($this->web->languages as $language)
        {
            $url = $this->web->api("/" . $language ."-page");
            $response = file_get_contents($url);
            file_put_contents($this->structurePath($language), $response);
            if ( $this->_language == $language )
            {
                $current = $response;
            }

        }
        return $current_language;
    }
    /**
    * Get Page Object, with all structure
    */
    public function structure()
    {
        if (file_exists($this->structurePath()))
        {
            $response = file_get_contents($this->structurePath());
        }
        else
        {
            $response = $this->structureDownload();
        }

        $page = json_decode($response);
        return $page;
    }
    /**
    * Build path to page json, structure data
    */
    private function nodePath()
    {
        $new_uri = str_replace("/", "__", $this->_uri);
        if (strpos($new_uri, ".amp") !== false)
        {
            $new_uri = str_replace(".amp", "", $new_uri);
            $this->_amp = true;
        }
        $path = $this->web->path("nodes" . $new_uri);
        return $path;
    }
    private function sanitizeUrl()
    {
        $current_language = "/" . $this->_language . "/";
        if ($this->_uri != "/" and $this->_uri != $current_language)
        {
            $this->_uri = str_replace("/", "_", $this->_uri);
            $this->_uri = str_replace("_".$this->_language."_", $current_language, $this->_uri);
            if ($this->_status == 404)
            {
                $this->_uri = "/not_found/" . $this->_uri;
            }
        }
    }
    /**
    * author: @erajuan
    * date: 30-MAY-2017
    * description: Download node
    */
    public function nodeDownload()
    {
        $url = $this->web->api("/p" . $this->_uri . "-json");
        $response = @file_get_contents($url);

        if ($response === FALSE) {
            $response = '{"status":400}';
        }

        $node = json_decode($response);
        if ($node->status == 200 )
        {
            file_put_contents($this->nodePath(), $response);
        } else {
            if ($node->status == 301 )
            {
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: " . $node->new_path);
                exit;
            }
        }
        return $node;
    }
    /**
    * Build a link for download node
    */
    public function node()
    {
        if ($this->_status === 403 )
        {
            $node = json_decode('{"status":403}');
            return $node;
        } else {
        if ( file_exists($this->nodePath()) )
        {
            $response = file_get_contents($this->nodePath());
            $node = json_decode($response);
            return $node;
        } else{
            return $this->nodeDownload();
        }
        }
    }
    /**
    * @erajuna
    * Return if this node support amp
    */
    public function isAmp()
    {
        return $this->_amp;
    }
}