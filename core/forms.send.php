<?php

require_once "web.php";
/**
* Utils
*/
class SendForm
{
    private $web_code;
    private $web_pk;
    private $api_url;

    function __construct($server_name)
    {
        $web = new Web($server_name);
        $web->info();

        $this->web_pk = $web->pk();
        $this->web_code = $web->code();
        $this->api_url = $web->api();
    }
    /**
    * Enviar formulario de contacto
    */
    public function contactForm($data)
    {
        $remove = array("http:", "https:", ".ly", "www", "/");
        $message = $data["message"];
        $data_string = "";
        $data_string .= "name=".$data["full_name"];
        $data_string .= "&email=".$data["email"];
        if (isset($data["phone"]))
        {
            $data_string .= "&phone=".$data["phone"];
        }
        $data_string .= "&subject=".$data["subject"];
        $data_string .= "&message=". $message;
        if (strpos($message, "http") === false) {
            if (strpos($message, ".ly") === false) {
                $res = $this->sendCURL("/webform/contact_us/",$data_string);
                return $res;
            }
        }
        return True;
    }
    /**
    * @author: @erajuan
    * @date: 15/05/2017
    * description: Enviar formulario de reserva de tour a la API
    */
    public function bookTour($data)
    {
        $data_string = "";
        $data_string .= "name=".$data["full_name"];
        $data_string .= "&email=".$data["email"];
        if (isset($data["phone"]))
        {
            $data_string .= "&phone=".$data["phone"];
        }
        if (isset($data["country"]))
        {
            $data_string .= "&country=".$data["country"];
        }
        if (isset($data["node_id"]))
        {
            $data_string .= "&tour_id=".$data["node_id"];
        }
        $data_string .= "&tour=".$data["tour"];
        $data_string .= "&journey_date=".$data["journey_date"];
        $data_string .= "&adults=".$data["adults"];
        $data_string .= "&children=".$data["children"];
        $data_string .= "&message=".$data["message"];

        $res = $this->sendCURL("/webform/book_tour/",$data_string);
        return $res;
    }
    /**
    * @author: @erajuan
    * @date: 15/05/2017
    * description: Enviar formulario de reserva de habitacion a la API
    */
    public function bookRoom($data)
    {
        $data_string = "";
        $data_string .= "name=".$data["full_name"];
        $data_string .= "&email=".$data["email"];
        if (isset($data["phone"]))
        {
            $data_string .= "&phone=".$data["phone"];
        }
        $data_string .= "&room=".$data["room_type"];
        $data_string .= "&check_in=".$data["check_in"];
        $data_string .= "&check_out=".$data["check_out"];
        $data_string .= "&adults=".$data["adults"];
        $data_string .= "&children=".$data["children"];
        $data_string .= "&message=".$data["message"];

        $res = $this->sendCURL("/webform/book_room/",$data_string);
        return $res;
    }
    /**
    * @author: @erajuan
    * @date: 15/05/2017
    * description: Registrar emails en suscription
    */
    public function suscription($data)
    {
        $data_string = "email=".$data["email"];

        $res = $this->sendCURL("/webform/book_room/",$data_string);
        return $res;
    }
    private function sendCURL($url, $data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->api_url . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);

        curl_close($curl);
        return array($response, $error);
    }

}
