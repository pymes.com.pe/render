<?php
/**
* Invoice
* autor: @erajuan
* company: pymes.com.pe
*/
require '../vendor/autoload.php';
require_once "config.php";
require_once "web.php";
require_once "page.php";

function renderSafe($field)
{
    return $field;
}
class Invoice
{
    private $robots_index; # Robot index
    private $web; # OBJECT: Domain, PK, Code, status
    # Domains status: 200-Exists, 404-Not register, 403-Suspende
    private $template; #
    private $language; # current language
    private $pk;
    private $uri;

    function __construct($server_name,$request_uri)
    {
        $this->uri = $request_uri;

        $web = new Web($server_name);
        $web->info();
        $this->web = $web;
    }
    function twig($ctx)
    {
        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem(TEMPLATES_DIR);
        $twig = new Twig_Environment($loader, array(
            'cache' => false,
            'auto_reload' => true
            ));
        $twig->disableDebug();
        $filterSafe = new Twig_SimpleFilter(
            'safe',
            'renderSafe',
            array('is_safe' => array('html')));
        $twig->addFilter($filterSafe);
        // Add context variables
        $ctx["static_url"] = STATIC_URL;
        // Verificar que existe node
        if ( $ctx["node"]->status == 200 )
        {
            try {
                $theme = $twig->render($this->web->template.'/invoice.twig.html', $ctx);
            } catch (Exception $e) {
                $theme = $twig->render('core/invoice.twig.html', $ctx);
            }
            return $theme;
        } else {
            $ctx["web"]->robot_index = false;
            return $twig->render($this->web->template.'/error.twig.html', $ctx);
        }
    }
    public function download()
    {
        $url = $this->web->api("/invoices/" . $this->pk);
        $response = @file_get_contents($url);

        if ($response === FALSE) {
            $response = '{"status":400}';
        }

        $node = json_decode($response);
        return $node;
    }
    public function charge($token_id)
    {
        try {
        $culqi = new Culqi\Culqi(array('api_key' => 'sk_test_CC2BcGNifeRysHT8'));
        // 3. Get structure object
        //$structure = $page->structure();
        //$node = $this->download();
        // Creando Cargo a una tarjeta
            $charge = $culqi->Charges->create(
                array(
                  "amount" => 1000,
                  "capture" => true,
                  "currency_code" => "PEN",
                  "description" => "Venta de prueba",
                  "email" => "test@culqi.com",
                  "installments" => 0,
                  "antifraud_details" => array(
                      "address" => "Av. Lima 123",
                      "address_city" => "LIMA",
                      "country_code" => "PE",
                      "first_name" => "Will",
                      "last_name" => "Muro",
                      "phone_number" => "9889678986",
                  ),
                  "source_id" => $token_id
                )
            );
            // Respuesta
            return json_encode($charge); //*/
        } catch (Exception $e) {
          print_r($e);
        }
    }
    public function start($invoice)
    {
        $i = explode('-', $invoice);
        $this->pk = $i[0] . '-' . $i[1] ;
        // 1. Verify if domain exist
        if ($this->web->status == 200)
        {
            $page = new Page(
                $this->web,
                $this->uri
                );
            // 3. Get structure object
            $structure = $page->structure();
            $node = $this->download();
            // render template
            $ctx = array(
                'web' => $this->web,
                'page' => $structure,
                'node' => $node,
                'amp' => false,
                );
            return $this->twig($ctx);
        }
        return $this->error();
    }
}